# Infracost GitLab CI

### Try the GitLab App
👉👉 We recommend using the [**free Infracost GitLab App**](https://www.infracost.io/docs/integrations/gitlab_app/) instead as it has many benefits over GitLab CI

---

This project provides a set of GitLab CI examples for Infracost, so you can see cloud cost estimates for Terraform in merge requests 💰

<img src="screenshot.png" alt="Example screenshot" width="800px" />

Follow our [migration guide](https://www.infracost.io/docs/guides/gitlab_ci_migration/) if you used our old version of this repo.

## Quick start

The following steps assume a simple Terraform directory is being used, we recommend you use a more relevant [example](#examples) if required.

1. If you haven't done so already, [download Infracost](https://www.infracost.io/docs/#quick-start) and run `infracost auth login` to get a free API key.

2. Retrieve your Infracost API key by running `infracost configure get api_key`.

3. Create a [project environment variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) called `INFRACOST_API_KEY` with your API key. This should be masked. To make sure this can be used on all merge requests untick the 'Protect variable' option.

4. Add a new [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), or [Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) (for premium tier/self-managed GitLab users). This access token should have the `api` scope enabled with Developer role, so it is able to read and write comments on merge requests. As in step 2, create a masked project environment variable called `GITLAB_TOKEN` and to make sure this can be used on all merge requests untick the 'Protect variable' option."

5. Create a new file in `.gitlab-ci.yml` in your repo with the following content.

    ```yaml
    variables:
      # If your terraform files are in a subdirectory, set TF_ROOT accordingly
      TF_ROOT: RELATIVE/PATH/TO/TERRAFORM/CODE # Update this!
    stages:
      - infracost

    infracost:
      stage: infracost
      image:
        # Always use the latest 0.10.x version to pick up bug fixes and new resources.
        # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
        name: infracost/infracost:ci-0.10
        entrypoint: [""]
      script:
        # If you use private modules, add an environment variable or secret
        # called GIT_SSH_KEY with your private key, so Infracost can access
        # private repositories (similar to how Terraform/Terragrunt does).
        # - mkdir -p ~/.ssh
        # - eval `ssh-agent -s`
        # - echo "$GIT_SSH_KEY" | tr -d '\r' | ssh-add -
        # Update this to github.com, gitlab.com, bitbucket.org, ssh.dev.azure.com or your source control server's domain
        # - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

        # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
        - git clone $CI_REPOSITORY_URL --branch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --single-branch /tmp/base

        # Generate an Infracost cost snapshot from the comparison branch, so that Infracost can compare the cost difference.
        - |
          infracost breakdown --path=/tmp/base/${TF_ROOT} \
                              --format=json \
                              --out-file=infracost-base.json

        # Generate an Infracost diff and save it to a JSON file.
        - |
          infracost diff --path=${TF_ROOT} \
                         --compare-to=infracost-base.json \
                         --format=json \
                         --out-file=infracost.json

        # Posts a comment to the PR using the 'update' behavior.
        # This creates a single comment and updates it. The "quietest" option.
        # The other valid behaviors are:
        #   update - Create a single comment and update it. The "quietest" option.
        #   delete-and-new - Delete previous comments and create a new one.
        #   new - Create a new cost estimate comment on every push.
        # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
        - |
          infracost comment gitlab --path=infracost.json \
                                   --repo=$CI_PROJECT_PATH \
                                   --merge-request=$CI_MERGE_REQUEST_IID \
                                   --gitlab-server-url=$CI_SERVER_URL \
                                   --gitlab-token=$GITLAB_TOKEN \
                                   --behavior=update
      variables:
        INFRACOST_API_KEY: $INFRACOST_API_KEY
        GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope and Developer role to post merge request comments
        # If you're using Terraform Cloud/Enterprise and have variables or private modules stored
        # on there, you can specify the following to automatically retrieve them:
        # INFRACOST_TERRAFORM_CLOUD_TOKEN: $INFRACOST_TERRAFORM_CLOUD_TOKEN
        # INFRACOST_TERRAFORM_CLOUD_HOST: app.terraform.io
      rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    ```

6. 🎉 That's it! Send a new merge request to change something in Terraform that costs money. You should see a merge request comment that gets updated, e.g. the 📉 and 📈 emojis will update as changes are pushed!

    If there are issues, check the GitLab CI logs and [this page](https://www.infracost.io/docs/troubleshooting/).

    <img src=".gitlab-ci/assets/mr-comment.png" alt="Example mr request" width="90%" />

7. In [Infracost Cloud](https://dashboard.infracost.io), go to Org Settings and enable the dashboard, then trigger your CI/CD pipeline again. This causes the CLI to send its JSON output to your dashboard; the JSON does not contain any cloud credentials or secrets, see the [FAQ](https://infracost.io/docs/faq/) for more information.

    This is our SaaS product that builds on top of Infracost open source. It enables team leads, managers and FinOps practitioners to setup [tagging policies](https://www.infracost.io/docs/infracost_cloud/tagging_policies/), [guardrails](https://www.infracost.io/docs/infracost_cloud/guardrails/) and [best practices](https://www.infracost.io/docs/infracost_cloud/cost_policies/) to help guide the team. For example, you can check for required tag keys/values, or suggest switching AWS GP2 volumes to GP3 as they are more performant and cheaper.

    <img src=".gitlab-ci/assets/infracost-cloud-dashboard.png" alt="Infracost Cloud gives team leads, managers and FinOps practitioners visibility across all cost estimates in CI/CD" width="58%" /><img src=".gitlab-ci/assets/pull-request-tags.png" alt="Communicate and enforce FinOps tags in merge requests" width="42%" />

### Troubleshooting

#### HTTP 403 error when posting comments

If you receive a 403 error when running the `infracost comment` command in your pipeline, check that the correct `$GITLAB_TOKEN` is being used; the environment variable [precedence order](https://about.gitlab.com/blog/2021/04/09/demystifying-ci-cd-variables/) docs might be helpful.

You can also use the following bash script to ensure that your token has the right access:

```sh
export GITLAB_TOKEN=mytoken

# If you're not using GitLab.com, set this to your server URL
export CI_SERVER_URL=https://gitlab.com

# Your repo path in the form of namespace/project with encoded slash character. If your repo path has a group, try the curl with and without the group, e.g. also try my-org%2my-group%2my-repo
export CI_PROJECT_PATH=my-org%2my-repo

# Set this to any merge request ID just so you can test the curl call
export CI_MERGE_REQUEST_IID=123

curl -i -X POST \
  --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
  --data-urlencode "body=testcomment" \
  $CI_SERVER_URL/api/v4/projects/$CI_PROJECT_PATH/merge_requests/$CI_MERGE_REQUEST_IID/notes

# If the response is HTTP 401 or 403, the token probably has issues
```

#### (Windows) No INFRACOST_API_KEY environment variable is set

If you run a pipeline using Windows gitlab-runner and receive "No INFRACOST_API_KEY environment variable is set", check that the env var is used with proper environment's format. For example, an env var should be used as `$INFRACOST_API_KEY` in Bash, but as `$env:INFRACOST_API_KEY` in PowerShell. See the official GitLab documentation to [learn more](https://docs.gitlab.com/ee/ci/variables/#use-cicd-variables-in-job-scripts).

#### (Windows) ParserError: Missing expression after unary operator '--'

If you run a pipeline using Windows gitlab-runner and receive ParseError, try to modify the multiline script CLI commands to be one-liners. For example, the following command

```sh
- |
  infracost breakdown --path=/tmp/base/${TF_ROOT} \
                      --format=json \
                      --out-file=infracost-base.json
```

should become

```sh

- |
  infracost breakdown --path=/tmp/base/${TF_ROOT} --format=json --out-file=infracost-base.json
```

## Examples

The [examples](examples) directory demonstrates how these actions can be used for different projects. They all work by using the default Infracost CLI option that parses HCL, thus a Terraform Plan JSON is not needed.
  - [Terraform/Terragrunt projects (single or multi)](examples/terraform-project): a repository containing one or more (e.g. mono repos) Terraform or Terragrunt projects
  - [Multi-projects using a config file](examples/multi-project-config-file): repository containing multiple Terraform projects that need different inputs, i.e. variable files or Terraform workspaces
  - [Private Terraform module](examples/private-terraform-module): a Terraform project using a private Terraform module
  - [Slack](examples/slack): send cost estimates to Slack

For advanced use cases where the estimate needs to be generated from Terraform plan JSON files, see the [plan JSON examples here](examples#plan-json-examples).

## Contributing

Issues and merge requests are welcome! For development details, see the [contributing](CONTRIBUTING.md) guide. For major changes, including interface changes, please open an issue first to discuss what you would like to change. [Join our community Slack channel](https://www.infracost.io/community-chat), we are a friendly bunch and happy to help you get started :)

If you'd like to contribute to this repo, you must [click here](https://cla-assistant.io/infracost/infracost) to sign our Contributor License Agreement manually as we have not yet setup that automation for GitLab.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
