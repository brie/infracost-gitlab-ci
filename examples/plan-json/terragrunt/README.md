# Terragrunt

This example shows how to run Infracost in GitLab CI with Terragrunt.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: plan-json-terragrunt)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/plan-json/terragrunt/code

stages:
  - plan
  - infracost

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform
    - ${TF_ROOT}/.terragrunt-cache

plan:
  stage: plan
  image:
    name: alpine/terragrunt:latest
    entrypoint: [""]
  before_script:
    # IMPORTANT: add any required steps here to setup cloud credentials so Terragrunt can run
    - cd ${TF_ROOT}
  script:
    # Generate plan JSON files for all Terragrunt modules and
    # add them to an Infracost config file
    - terragrunt run-all --terragrunt-ignore-external-dependencies plan -out=plan.cache
    - |
      # Find the plan files
      plans=($(find . -name plan.cache | tr '\n' ' '))

      # Generate plan JSON files by running terragrunt show for each plan file
      planjsons=()
      for plan in "${plans[@]}"; do
        # Find the Terraform working directory for running terragrunt show
        # We want to take the dir of the plan file and strip off anything after the .terraform-cache dir
        # to find the location of the Terraform working directory that contains the Terraform code
        dir=$(dirname $plan)
        dir=$(echo "$dir" | sed 's/\(.*\)\/\.terragrunt-cache\/.*/\1/')

        echo "Running terragrunt show for $(basename $plan) for $dir";
        terragrunt show -json $(basename $plan) --terragrunt-working-dir=$dir --terragrunt-no-auto-init > $dir/plan.json
        planjsons=(${planjsons[@]} "$dir/plan.json")
      done

      # Sort the plan JSONs so we get consistent project ordering in the config file
      IFS=$'\n' planjsons=($(sort <<<"${planjsons[*]}"))

      # Generate Infracost config file
      echo -e "version: 0.1\n\nprojects:\n" > infracost.yml
      for planjson in "${planjsons[@]}"; do
        echo -e "  - path: ${TF_ROOT}/$planjson" >> infracost.yml
      done
  artifacts:
    paths:
      # Add the Infracost config file and the generated plan JSONs to the artifacts
      - ${TF_ROOT}/infracost.yml
      - ${TF_ROOT}/**/plan.json
    exclude:
      # We can skip searching these paths for the plan JSONs
      - ${TF_ROOT}/modules
      - ${TF_ROOT}/**/.terragrunt-cache
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10 version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan
  script:
    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --config-file=${TF_ROOT}/infracost.yml \
                     --format=json \
                     --out-file=infracost.json

    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

```
[//]: <> (END EXAMPLE)
